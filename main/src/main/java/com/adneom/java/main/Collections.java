package com.adneom.java.main;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by ccr on 07/11/2017.
 */
public class Collections {
    public static void main(String[] args) {
        Set<Integer> ints = Set.of(1,3432,300,-54,5,565,12232);
        int max = ints.stream()
                .max(Integer::compare)
                .get();
        System.out.println(max);

        Set<Integer> intSet = Map.of("a",1,"b",2)
                .entrySet()
                .stream()
                .peek(entry -> System.out.println("peek "+entry))
                .map(entry -> entry.getValue())
                .collect(Collectors.toSet());

        System.out.println(intSet);

    }
}
