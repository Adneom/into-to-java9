package com.adneom.java.main;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Flow;
import java.util.concurrent.SubmissionPublisher;
import java.util.stream.IntStream;

public class Reactive {

    public static void main(String[] args) {
        System.out.println("reactive example");

        ExecutorService executor = Executors.newFixedThreadPool(2);

        SubmissionPublisher<String> publisher = new SubmissionPublisher<>(executor, Flow.defaultBufferSize());
        publisher.subscribe(new BasicSubscriber("Sub1"));
        publisher.subscribe(new BasicSubscriber("Sub2"));

        IntStream.of(1,2,3,4,5).forEach(i -> publisher.submit("message"+i));

        //publisher.close();
        executor.shutdown();
    }



    static class BasicSubscriber implements Flow.Subscriber<String> {

        private String name;

        private Flow.Subscription subscription;
        private List<String> receivedElement = new ArrayList<>();

        BasicSubscriber(String name) {
            this.name = name;
        }

        @Override
        public void onSubscribe(Flow.Subscription subscription) {
            this.subscription=subscription;
            subscription.request(1);
            System.out.println(name+" onSubscribe");
        }

        @Override
        public void onNext(String item) {
            receivedElement.add(item);
            subscription.request(1);
        }

        @Override
        public void onError(Throwable throwable) {
            throw new RuntimeException(throwable);
        }

        @Override
        public void onComplete() {
            System.out.println(String.format("%s : %s",name, receivedElement));
        }
    }

}
