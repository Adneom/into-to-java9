package com.adneom.java.main;

public class Walker {

    public static void main(String[] args) {
        System.out.println("main");
        top();
    }

    public static void top() {
        System.out.println("top");
        middle();
    }

    public static void middle() {
        System.out.println("middle");
        low();
    }

    public static void low() {
        System.out.println("low");
        StackWalker sw = StackWalker.getInstance();
        sw.forEach(System.out::println);
    }

}
