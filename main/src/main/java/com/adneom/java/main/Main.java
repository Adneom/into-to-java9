package com.adneom.java.main;

import com.adneom.java.lib.Utils;
import com.adneom.service.HelloService;

import java.util.Optional;
import java.util.ServiceLoader;

public class Main {

    public static void main(String[] args) {
        Utils.sayHello();

        Optional<HelloService> firstService = ServiceLoader
                .load(HelloService.class)
                .findFirst();

        HelloService service = firstService
                .orElse(new HelloService() {});

        System.out.println(service.sayHiTo("toto"));
        System.out.println(new HelloService() {}.sayHiTo("toto"));
    }

}
