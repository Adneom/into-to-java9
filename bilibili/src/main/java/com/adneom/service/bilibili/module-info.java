module bilibili {
    requires services.hello.api;
    exports com.adneom.service.bilibili;
    provides com.adneom.service.HelloService
            with com.adneom.service.bilibili.HelloBilibiliService;
}