package com.adneom.service.bilibili;

import com.adneom.service.HelloService;

import static java.lang.String.format;

public class HelloBilibiliService implements HelloService {

    @Override
    public String sayHiTo(String name) {
        return format("••BiliBili %s !••",name);
    }
}
