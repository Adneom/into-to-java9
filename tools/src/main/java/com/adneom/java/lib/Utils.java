package com.adneom.java.lib;

public final class Utils {

    private Utils() throws InstantiationException {
        throw new InstantiationException(String.format("%s is an utility class", getClass().getName()));
    }

    public static void sayHello() {
        System.out.println("Hello world !");
    }

}
