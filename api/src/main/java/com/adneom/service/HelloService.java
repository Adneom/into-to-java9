package com.adneom.service;

import static java.lang.String.format;

public interface HelloService {

    default String sayHiTo(String name) {
        return decorate(format("Hello %s ! welcome to java 9",name));
    }

    private String decorate(String str) {
        return format("{{{%s}}}",str);
    }

}
